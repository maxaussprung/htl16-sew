const express = require('express');
const router = express.Router();
const db = require('../model/db');

/* GET home page. */
router
  .get('/', function (req, res, next) {
    res.render('employees', { data: db.data });
  })
  .post('/', function (req, res, next) {
    let body = JSON.parse(req.body.string);
    db.add(body.dep, body.user);
    res.send('res.status(200)');
  })
  .put('/', function (req, res, next) {
    let body = JSON.parse(req.body.string);
    db.update(body.dep, body.user);
    res.send('res.status(200)');
  });

module.exports = router;

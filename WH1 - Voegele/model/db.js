const fs = require('fs');

let data = JSON.parse(fs.readFileSync('emp.json', 'utf-8'));

function add(dep, user) {
    data[dep].push(user);
}

function update(dep, user){
    let index = data[dep].findIndex( cuser => cuser.firstName == user.firstName && cuser.lastName == user.lastName);
    data[dep][index].active = user.active;
}

module.exports = {
    data, add, update
}
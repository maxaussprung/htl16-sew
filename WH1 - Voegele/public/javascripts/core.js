function AddUser(body) {
    body = JSON.parse(body);
    $('tbody').append(`<tr><td>${body.user.firstName}</td> <td>${body.user.lastName}</td> <td>${body.user.firstName}</td> <td>${body.dep}</td></tr>`);
}

$(document).ready(async () => {

    $('#add').click(async () => {
        let body = JSON.stringify({
            user: {
                firstName: $('#firstname').val(),
                lastName: $('#lastname').val(),
                birthDate: $('#birthdate').val(),
                active: true
            },
            dep: $('#department option:selected').val()
        });

        try {
            await $.ajax({
                url: '/',
                method: 'post',
                data: { string: body }
            });
            AddUser(body);
        }
        catch (err) {
            console.log(err);
        }
    });

    $('tr').click(async function () {
        try {
            $(this).toggleClass('disabled');

            let body = JSON.stringify({
                user: {
                    firstName: $(this).find('td:eq(0)').text(),
                    lastName: $(this).find('td:eq(1)').text(),
                    birthDate: $(this).find('td:eq(2)').text(),
                    active: ($(this).hasClass('disabled')) ? false : true
                },
                dep: $(this).find('td:eq(3)').text()
            });

            //console.log(body);
            await $.ajax({
                url: '/',
                method: 'put',
                data: { string: body }
            });
        }
        catch (err) {
            console.log(err);
        }
    });

    $('#show').click(() => {
        let tr = $('tr');
        for (let item of tr)
            if ($(item).hasClass('disabled'))
                if (!$(item).hasClass('display'))
                    $(item).addClass('display');
                else
                    $(item).removeClass('display');
            else
                $(item).removeClass('display');
    });
});
const routes = require('express').Router();
var data = require('../model/model');

routes
  .get('/', (req, res) => {
    res.render('../views/mainview', data);
  })
  .post('/', (req, res) => {
    console.log(req.body);
    res.status(200).json({ message: 'Connected!' });
  });

module.exports = routes;
document.getElementById("button").addEventListener("click", async function (e) {
    let hero = {}
    for (e of document.getElementById("form").elements)
        if (e.type == "text")
            hero[e.name] = e.value;

    let res = await fetch('/', {
        method: 'post', 
        body: JSON.stringify(hero), 
        headers: { "Content-Type": "application/json" }
    });
    console.log(res, hero);
});
const express = require('express');
const bodyParser = require('body-parser');
const routes = require('./routes/route');

const app = express();

app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static('public'));

app.use('/', routes);

app.listen(3000, () => {
    console.log('App listening on port 3000');
});
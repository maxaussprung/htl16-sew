function card_template(board) {
    return `<div class="col col-lg-4 mb-3">
<div class="card" style="width: 18rem;">
    <!-- <img class="card-img-top" src=".../100px180/?text=Image cap" alt="Card image cap"> -->
    <div class="card-body">
        <h5 class="card-title">${board.item}</h5>
        <p class="card-text">Type: <span class="type">${board.type}</span></p>
        <p class="card-text">Brand: <span class="brand">${board.brand}</span></p>
        <p class="card-text">Edition: <span class="edition">${board.edition}</span></p>
        <p class="card-text">Price: <span class="price">${board.price}€</span></p>
    </div>
    <ul class="list-group list-group-flush">
       ${list_gen(board.sizes)}
    </ul>
    <div class="card-body cl display">
        <a href="#" class="card-link remove">Remove</a>
        <a href="#" class="card-link cedit" data-toggle="modal" data-target="#exampleModal">Edit</a>
    </div>
</div>
</div>`
}

function form(col) {
    if (col) {
        $('#type').val(col.find('.type').text());
        $('#brand').val(col.find('.brand').text());
        $('#item').val(col.find('h5').text());
        $('#item').attr('data',col.find('h5').text());
        $('#edition').val(col.find('.edition').text());
        $('#price').val(col.find('.price').text());
    } else return {
        "type": $('#type').val(),
        "brand": $('#brand').val(),
        "item": $('#item').val(),
        "edition": $('#edition').val(),
        "sizes": [],
        "price": $('#price').val(),
        "oitem": $('#item').attr('data')
    }
}

function form_clean(){
    $('#type').val('');
    $('#brand').val('');
    $('#item').val('');
    $('#edition').val('');
    $('#price').val('');
}

function list_gen(sizes) {
    let list = '';
    for (let size of sizes)
        list += `<li class="list-group-item">${size}</li>`;
    return list;
}

async function cards_build() {
    $( ".row" ).empty();

    let data = await $.ajax({
        method: 'get',
        url: '/api/snowboard'
    });
    $('h1').text(data.name);

    for (let board of data.boards)
        $('.row').append(card_template(board));
}

$(document).ready(async () => {

    await cards_build();

    $('.remove').click(async function () {
        try {
            let col = $(this).closest('.col');
            let board = col.find('h5').text();

            await $.ajax({
                method: 'delete',
                url: `/api/snowboard/${board}`
            });

            col.remove();
        } catch (err) {
            throw err;
        }
    });

    $('#submit').on('click', async function () {
        await $.ajax({
            method: 'post',
            url: '/api/snowboard',
            data: form()
        });
        form_clean();
        cards_build();
    });

    $('.cedit').on('click', function () {
        let col = $(this).closest('.col');
        form(col);
    });

    $('#editmode').on('click', () => {
       $('.cl').toggleClass('display');
    });
});
const routes = require('express').Router();
const db = require('../model/model');

routes
  //get route
  .get('/', (req, res) => {
    res.send(db.data);
  })
  //delete route
  .delete('/:board',(req, res) => {
    db.remove(req.params.board);
    res.sendStatus(200);
  })
  //post and put in one to make my life easier
  .post('/',(req, res) => {
    db.push(req.body);
    res.sendStatus(200);
  })

module.exports = routes;
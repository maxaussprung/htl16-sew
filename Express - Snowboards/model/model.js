const fs = require('fs');
let data = JSON.parse(fs.readFileSync('./model/snwbrd.json', 'utf8'));

function remove(board){
    data.boards = data.boards.filter(b => b.item != board);
}

function push(board){
    remove(board.oitem);
    //temp-
    board.sizes = ['150'];
    //-----
    data.boards.push(board);
}

module.exports = { data, remove, push};
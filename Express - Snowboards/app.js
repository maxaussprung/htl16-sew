const express = require('express');
const bodyParser = require('body-parser');
const routes = require('./routes/route');

const PORT = 3000;

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static('public'));

app.use('/api/snowboard', routes);

app.listen(PORT, () => {
    console.log(`App listening on port ${PORT}`);
});